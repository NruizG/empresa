/**
 * Funcionalidades.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    id: {
      type: 'number',
      unique: true,
      allowNull: false,
      autoIncrement: true
    },
    description: {
      type: 'string',
      allowNull: false
    },
    backlog: {
      model: 'backlogs'
    },
    developer: {
      model: 'desarrolladores'
    },
    limitDate: {
      type: 'number',
      allowNull: false
    }
  },

};

