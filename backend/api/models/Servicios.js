/**
 * Servicios.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    id: {
      type: 'number',
      unique: true,
      allowNull: false,
      autoIncrement: true
    },
    name: {
      type: 'string',
      allowNull: false
    },
    hourCost: {
      type: 'number',
      allowNull: false
    },
    developer: {
      model: 'desarrolladores'
    }


  },

};

