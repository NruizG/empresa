/**
 * Backlogs.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    id: {
      type: 'number',
      unique: true,
      allowNull: false,
      autoIncrement: true
    },
    name: {
      type: 'string',
      allowNull: false
    },
    prerequisites: {
      type: 'string',
      allowNull: true
    },
    subRequirement: {
      type: 'boolean',
      allowNull: false
    }
  },

};

